import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { LoadingController, ToastController } from '@ionic/angular';
import { ReclamacoesService } from 'src/app/services/reclamacoes.service';
import { Reclamacoes } from 'src/app/interfaces/reclamacoes';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-reclamacao',
  templateUrl: './reclamacao.page.html',
  styleUrls: ['./reclamacao.page.scss'],
})
export class ReclamacaoPage implements OnInit {
  private loading: any;
  public Reclamacao = new Array<Reclamacoes>();
  private reclamacaoSubscription: Subscription;


  constructor(
    private authService: AuthService,
    private loadingCtrl: LoadingController,
    private reclamacoesService: ReclamacoesService,
    private toastCtrl: ToastController
  ) {
    this.reclamacaoSubscription = this.reclamacoesService.getReclamacao().subscribe(data => {
      this.Reclamacao = data;
    });
  }

  ngOnInit() { }

  ngOnDestroy() {
    this.reclamacaoSubscription.unsubscribe();
  }

  async logout() {
    await this.presentLoading();

    try {
      await this.authService.logout();
    } catch (error) {
      console.error(error);
    } finally {
      this.loading.dismiss();
    }
  }

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({ message: 'Aguarde...' });
    return this.loading.present();
  }

  async deleteReclamacoes(id: string) {
    try {
      await this.reclamacoesService.deleteReclamacoes(id);
    } catch (error) {
      this.presentToast('Erro ao tentar deletar');
    }
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({ message, duration: 2000 });
    toast.present();
  }
}