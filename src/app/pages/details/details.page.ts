import { Component, OnInit } from '@angular/core';
import { ReclamacoesService } from 'src/app/services/reclamacoes.service';
import { ActivatedRoute } from '@angular/router';
import { Reclamacoes } from 'src/app/interfaces/reclamacoes';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  private reclamacoesId: string = null;
  public reclamacoes: Reclamacoes = {};
  private loading: any;
  private reclamacoesSubscription: Subscription;

  constructor(
    private reclamacoesService: ReclamacoesService,
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private authService: AuthService,
    private toastCtrl: ToastController
  ) {
    this.reclamacoesId = this.activatedRoute.snapshot.params['id'];

    if (this.reclamacoesId) this.loadReclamacoes();
  }

  ngOnInit() { }

  ngOnDestroy() {
    if (this.reclamacoesSubscription) this.reclamacoesSubscription.unsubscribe();
  }

  loadReclamacoes() {
    this.reclamacoesSubscription = this.reclamacoesService.getReclamacoes(this.reclamacoesId).subscribe(data => {
      this.reclamacoes = data;
    });
  }

  async saveReclamacoes() {
    await this.presentLoading();

    this.reclamacoes.userId = this.authService.getAuth().currentUser.uid;

    if (this.reclamacoesId) {
      try {
        await this.reclamacoesService.updateReclamacoes(this.reclamacoesId, this.reclamacoes);
        await this.loading.dismiss();

        this.navCtrl.navigateBack('/reclamacao');
      } catch (error) {
        this.presentToast('Erro ao tentar salvar');
        this.loading.dismiss();
      }
    } else {
      this.reclamacoes.createdAt = new Date().getTime();

      try {
        await this.reclamacoesService.addReclamacoes(this.reclamacoes);
        await this.loading.dismiss();

        this.navCtrl.navigateBack('/reclamacao');
      } catch (error) {
        this.presentToast('Erro ao tentar salvar');
        this.loading.dismiss();
      }
    }
  }

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({ message: 'Aguarde...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({ message, duration: 2000 });
    toast.present();
  }
}