export interface Reclamacoes {
    id?: string;
    name?: string;
    descricao?: string;
    email?: string;
    telefone?: string;
    endereco?: string;
    createdAt?: number;
    userId?: string;
}