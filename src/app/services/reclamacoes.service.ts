import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Reclamacoes } from '../interfaces/reclamacoes';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReclamacoesService {
  private reclamacaoCollection: AngularFirestoreCollection<Reclamacoes>;

  constructor(private afs: AngularFirestore) {
    this.reclamacaoCollection = this.afs.collection<Reclamacoes>('Reclamacao');
  }

  getReclamacao() {
    return this.reclamacaoCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;

          return { id, ...data };
        });
      })
    );
  }

  addReclamacoes(reclamacoes: Reclamacoes) {
    return this.reclamacaoCollection.add(reclamacoes);
  }

  getReclamacoes(id: string) {
    return this.reclamacaoCollection.doc<Reclamacoes>(id).valueChanges();
  }

  updateReclamacoes(id: string, reclamacoes: Reclamacoes) {
    return this.reclamacaoCollection.doc<Reclamacoes>(id).update(reclamacoes);
  }

  deleteReclamacoes(id: string) {
    return this.reclamacaoCollection.doc(id).delete();
  }
}