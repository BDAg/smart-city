// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBI4MzQu6Xp_LW7BKxG0p4bvSAU7DMrJ5I",
    authDomain: "smartcity-15571.firebaseapp.com",
    databaseURL: "https://smartcity-15571.firebaseio.com",
    projectId: "smartcity-15571",
    storageBucket: "smartcity-15571.appspot.com",
    messagingSenderId: "68347053944",
    appId: "1:68347053944:web:997c120a0299013f"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
